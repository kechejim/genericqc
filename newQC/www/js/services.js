angular.module('starter.services', [])
.value('Mainhost', 'http://localhost/slim-test/')
//.value('Mainhost', 'http://dev.roadlogica.com/genericqc_server/api/api.php/')
.value('AppName', 'surveyqc1')
/**
 * A simple example service that returns some data.
 */ 
.factory('Pages',['Mainhost', 'AppName', '$http', '$rootScope', function(Mainhost, AppName, $http, $rootScope) {
  // Might use a resource here that returns a JSON array

  var pageList = [];	

  return {
  	LoadApp: function(jsonfile) {
		$http({method: 'GET', url: Mainhost + 'getappinfo' ,  params: {appname: jsonfile}} )
			.success(function(data, status, headers, config) {		

				console.log(data.pageist);	

			    pageList.splice(0, pageList.length);
			    for (var page in data.pagelist) {
                	
                	var pageinfo = data.pagelist[page];
                	pageinfo['title'] = page;                       

                	pageList.push(pageinfo);
                }		

				return true;
			})
			.error(function(data, status, headers, config) {

				console.log('errr');
				return null;
			});	
  	},
    all: function() {
      return pageList;
    },
    get: function(pageId) {
      return pageList[pageId];
	}
  };

}])
.factory('api',['Mainhost','$http', 'AppName', function(Mainhost, $http, AppName) {
  
  //
  // rest api interface
  //
          
  var getApp = function() {
    return $http({method: 'GET', url: Mainhost + 'getappinfo' ,  params: {appname: 'surveyqc1'}} )
					.success(function(data, status, headers, config) {			
						
						console.log(data);

						return data;
					})
					.error(function(data, status, headers, config) {
						//	$scope.name = 'Error!'

						console.log('errr');

						return null;
					});	
  },

  getSurveyList = function(callback) {
    return $http({method: 'GET', url: Mainhost + 'surveys' } )
					.success(function(data, status, headers, config) {			
						
						console.log(data);

						callback(data);
					})
					.error(function(data, status, headers, config) {
						//	$scope.name = 'Error!'

						console.log('errr');

						callback(null);
					});	
  },
  sendreport = function(data) {
    return $http({
    				method: 'POST', 
    				url: Mainhost + 'submitreport' ,  
    				data: { 
    						data : data,
    						app : AppName
    				}, 	
    				headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    } 
                }).success(function(data, status, headers, config) {			
						
						alert(data);;
						console.log('report : ' + data);

						return data;
					})
					.error(function(data, status, headers, config) {
						//	$scope.name = 'Error!'

						console.log('errr');

						return null;
					});	
  };

  return {    
    getApp  : getApp,
  	sendreport : sendreport,
	getSurveyList : getSurveyList	
  };

}]);

