angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope, $rootScope, Pages, api) {


	api.getSurveyList(function(data) {
		
		$scope.surveys = data;	

	});
	
	$scope.selectSurvey = function(d) {
		alert(d.id);
		console.log(d);
		
		$rootScope.isAppSelected = true;
		$rootScope.selectedApp = d;

		Pages.LoadApp(d.appname);
      
	};
	/*
	$scope.sendReport = function() {
	
		var pages = Pages.all();
		var report = {};
	
		for (var i=0; i< pages.length; i++) {

			report[pages[i].title] = pages[i];     	
        }
		
		if (api.sendreport(report)) {
			alert('Submitted successfully !');
		}

	};*/
})

.controller('PagesCtrl', function($scope, $rootScope, Pages, api) {

	//load page info from server
	if ($rootScope.isAppSelected == false) {
		$scope.pages = null;
	}
	else {
		$scope.pages = Pages.all();
	}

	console.log($scope.pages);
})

.controller('PageDetailCtrl', function($scope, $stateParams, Pages, $cordovaCapture, $cordovaCamera, $ionicPopup) {

	$scope.page = Pages.get($stateParams.pageId);
	console.log($scope.page);

	//option for image capture
	$scope.showPopup = function(k) {

		var myPopup = $ionicPopup.show({
			template: '',
			title: 'Confirm',
			subTitle: 'Please select "From File" or "From Camera"',
			scope: $scope,
			buttons: [ 
				{
					text: 'Camera Capture',
					type: 'button-positive',
					onTap: function(e) {
					
						$scope.captureImageFromCamera(function(imageData){

							$scope.page.fields[k].value = 'data:image/png;base64,' + imageData;
							
						});

					}
				},
				{
					text: 'File Load',
					type: 'button-default',
					onTap: function(e){

						$scope.captureImageFromFile(function(imageData) {

							$scope.page.fields[k].value = 'data:image/png;base64,' + imageData;
							
						});
					}			
				}				
			]
		});
	
		myPopup.then(function(res) {
			console.log('Tapped!', res);
		});

	};


	//
	// functions for capture image
	//
	$scope.captureImageFromFile = function(callback) {

		var options = { 
			quality : 75, 
			destinationType : Camera.DestinationType.DATA_URL, 
			sourceType : Camera.PictureSourceType.PHOTOLIBRARY,
			allowEdit : true,
			encodingType: Camera.EncodingType.PNG,
			targetWidth: 1024,
			popoverOptions: CameraPopoverOptions,
			saveToPhotoAlbum: false
		};

		$cordovaCamera.getPicture(options).then(function(imageData) {

		  callback(imageData);
		}, function(err) {

		  alert('error');
		});
	};

	$scope.captureImageFromCamera = function(callback) {

		var options = { 
			quality : 75, 
			destinationType : Camera.DestinationType.DATA_URL, 
			sourceType : Camera.PictureSourceType.CAMERA, 
			allowEdit : true,
			encodingType: Camera.EncodingType.PNG,
			targetWidth: 1024,
			popoverOptions: CameraPopoverOptions,
			saveToPhotoAlbum: false
		};

		$cordovaCamera.getPicture(options).then(function(imageData) {

		  callback(imageData);
		}, function(err) {

		  alert('error');
		});
	};


})

.controller('AccountCtrl', function($scope) {


});
